**The 5 NoSQL databases are**-

1.MongoDB

2.Apache Cassandra

3.Apache HBase

4.Apache CouchDB

5.Neo4j



_**Detailed information about these NoSQL databases**._



**MongoDB**-- MongoDB is an object-oriented, simple, dynamic, and scalable NoSQL database. It is
based on the NoSQL document store model. The data objects are stored as separate documents 
inside a collection instead of storing the data into the columns and rows of a traditional 
relational database. MongoDB uses JSON-like documents with schemas.MongoDB supports field, 
range query, and regular-expression searches.Queries can return specific fields of documents 
and also include user-defined JavaScript functions. Queries can also be configured to return 
a random sample of results of a given size. 

_****Use of MongoDB****_-- 

A. MongoDB is a tool that can manage document-oriented information, store or retrieve 
information.

B. MongoDB is built on a scale-out architecture that has become popular with developers of 
all kinds for developing scalable applications with evolving data schemas.







**Apache Cassandra**-- Apache Cassandra is the only distributed NoSQL database that delivers the 
always-on availability, blisteringly fast read-write performance, and unlimited linear 
scalability needed to meet the demands of successful modern applications.Apache Cassandra is a
distributed NoSQL database created at Facebook and later released as an open-source project 
in July 2008.Cassandra was designed to implement a combination of Amazon's Dynamo distributed 
storage and replication techniques combined with Google's Bigtable data and storage engine 
model.


_**Use of Apache Cassandra**_--

A.Cassandra excels at storing time-series data, where old data does not need to be updated.

B.Cassandra can be cost effective when network (e.g. moving data around data centres) costs 
are high because it does not need to keep sending data to a far-away master node.




**Apache HBase-**- HBase is a data model that is similar to Google’s big table. It is an open 
source, distributed database developed by Apache software foundation written in Java. HBase is 
an essential part of our Hadoop ecosystem. HBase runs on top of HDFS (Hadoop Distributed File 
System). It can store massive amounts of data from terabytes to petabytes. It is column 
oriented and horizontally scalable.HBase features compression, in-memory operation, and Bloom 
filters on a per-column basis as outlined in the original Bigtable paper.Tables in HBase 
can serve as the input and output for MapReduce jobs run in Hadoop, and may be accessed through
the Java API but also through REST, Avro or Thrift gateway APIs.

_**Use of Apache HBase**_--

A.It enables random, strictly consistent, real-time access to petabytes of data.

B.Apache Phoenix is commonly used as a SQL layer on top of HBase allowing you to use familiar 
SQL syntax to insert, delete, and query data stored in HBase.




**Apache CouchDB**-- Apache CouchDB is an open source NoSQL document database that collects and stores data in JSON-based document formats. Unlike relational databases, CouchDB uses a schema-free data model, which simplifies record management across various computing devices, mobile phones, and web browsers.CouchDB comes with a suite of features, such as on-the-fly document transformation and real-time change notifications, that make web development a breeze. It even comes with an easy to use web administration console, served directly out of CouchDB! We care a lot about distributed scaling. CouchDB is highly available and partition tolerant, but is also eventually consistent. And we care a lot about your data. 


_**Use of Apache CouchDB**_--

A.CouchDB uses a schema-free data model, which simplifies record management across various computing devices, mobile phones, and web browsers.

B. CouchDB is used as multi-node peer-to-peer offline-first database. IBM Cloud services are based at a fundamental level on CouchDB. 






**Neo4j**-- Neo4j is a graph database management system developed by Neo4j, Inc. Described by its 
developers as an ACID-compliant transactional database with native graph storage and processing
,Neo4j is available in a non-open-source "community edition" licensed with a modification 
of the GNU General Public License, with online backup and high availability extensions licensed
under a closed-source commercial license.Neo also licenses Neo4j with these extensions 
under closed-source commercial terms.Neo4j is implemented in Java and accessible from software
written in other languages using the Cypher query language through a transactional HTTP 
endpoint, or through the binary "Bolt" protocol.

_**Use of Neo4j**_--

A.Neo4j delivers the lightning-fast read and write performance you need, while still protecting
your data integrity.

B.It is more suitable for certain big data and analytics applications than row and column
databases or free-form JSON document databases for many use cases.
